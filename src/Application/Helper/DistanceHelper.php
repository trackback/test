<?php

namespace Core\Application\Helper;

class DistanceHelper
{
    /**
     * @var int
     */
    private $earthRadius = 6371;

    /**
     * @param $latitudeOfLocation
     * @param $longitudeOfLocation
     * @param $latitudeOfDestination
     * @param $longitudeOfDestination
     * @return float
     */
    public function getDistance(
        $latitudeOfLocation,
        $longitudeOfLocation,
        $latitudeOfDestination,
        $longitudeOfDestination
    ) {
        $latFrom = deg2rad($latitudeOfLocation);
        $lonFrom = deg2rad($longitudeOfLocation);
        $latTo   = deg2rad($latitudeOfDestination);
        $lonTo   = deg2rad($longitudeOfDestination);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                               cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $this->earthRadius;
    }
}
