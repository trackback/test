<?php

namespace Core\Application\DataProcessing;

use Core\Application\DataProcessing\Import\Source\ImportSourceInterface as ImportSource;
use Core\Application\DataStorage\DataStorageInterface as DataStorage;
use InvalidArgumentException;
use LengthException;
use RuntimeException;
use UnexpectedValueException;

class ImportObjectPoints
{
    /**
     * @var ImportSource $source
     */
    private $source;

    /**
     * @var DataStorage $objectPointsStorage
     */
    private $objectPointsStorage;

    /**
     * @param ImportSource $source
     * @param DataStorage  $objectPointsStorage
     */
    public function __construct(ImportSource $source, DataStorage $objectPointsStorage)
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);

        $this->source              = $source;
        $this->objectPointsStorage = $objectPointsStorage;
    }

    /**
     * @param array $fieldsSet
     * @param int   $chunkSize
     * @param bool  $skipHeader
     */
    public function import(array $fieldsSet, int $chunkSize = 100, bool $skipHeader = false)
    {
        if ($this->source->isEmpty()) {
            throw new LengthException("Data not found in input source");
        }

        if ($fieldsSet == []) {
            throw new InvalidArgumentException("Empty fieldset");
        }

        if ($chunkSize < 1) {
            throw new UnexpectedValueException("Chunk size to less");
        }

        if ($skipHeader) {
            $this->source->getOne();
        }

        while ($this->source->hasNextRow() !== false) {
            $objectsPoints = $this->readChunk($chunkSize);
            $objects       = [];

            array_filter($objectsPoints, function ($dataArray) use (&$objects, $fieldsSet) {
                $attributesValues = array_intersect_key($dataArray, $fieldsSet);
                $objectPoints     = array_combine(array_values($fieldsSet), array_values($attributesValues));
                $objects[]        = $objectPoints;
            });

            $this->objectPointsStorage->createMultiple($objects);
        }
    }

    /**
     * @param int $count
     * @return array
     */
    private function readChunk(int $count)
    {
        $counter   = 0;
        $usersData = [];

        while ($this->source->hasNextRow() !== false && $counter !== $count) {
            $usersData[] = $this->source->getOne();
            $counter++;
        }

        return $usersData;
    }

    public function __destruct()
    {
        ini_set('max_execution_time', 30);
        set_time_limit(30);
    }
}
