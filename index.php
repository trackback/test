<?php

ini_set('display_errors', 1);

require_once('vendor/autoload.php');

use Core\Application\Config\MysqlConfig;
use Core\Application\DataProcessing\Import\Source\FileSource;
use Core\Application\DataProcessing\ImportObjectPoints;
use Core\Application\DataStorage\Source\DatabaseStorage;
use Core\Application\DataStorage\ObjectPointsStorage;
use Core\Application\FileProcessing\Service\FilesProcessingService;
use Core\Application\FileProcessing\Source\CSVFileSource;
use Core\Application\Helper\DistanceHelper;
use Core\Application\Source\PdoAdapter;

$mysqlConnectionConfig = new MysqlConfig('localhost', '5432', '123456', 'root', 'test');

$mysqlAdapter    = new PdoAdapter($mysqlConnectionConfig);
$databaseStorage = new DatabaseStorage('objects', $mysqlAdapter);
$dataSource      = new ObjectPointsStorage($databaseStorage);

if (isset($_GET['action'])) {
    if ($_GET['action'] === 'import') {
        echo "Begin import coordinates";
        $fileSource      = new CSVFileSource(__DIR__.DIRECTORY_SEPARATOR.'data.csv');
        $filesProcessing = new FilesProcessingService();
        $filesProcessing->openFile($fileSource);
        $fileSourceForImport = new FileSource($filesProcessing);
        $import              = new ImportObjectPoints($fileSourceForImport, $dataSource);
        $import->import([0 => 'name', 1 => 'point'], 100, true);
        echo "Done";
    } elseif ($_GET['action'] === 'find') {
        $placeId     = (int)$_GET['place'];
        $radius      = (int)$_GET['radius'];
        $placeCoords = $dataSource->findById($placeId);
        $allPlaces   = $dataSource->findAll(['id != 0']);

        $distanceHelper = new DistanceHelper();

        $objects = [];

        foreach ($allPlaces as $place) {
            $locationCoords    = explode(',', $placeCoords['point']);
            $destinationCoords = $place['point'];

            $distance = $distanceHelper->getDistance(
                $locationCoords[0],
                $locationCoords[1],
                $destinationCoords[0],
                $destinationCoords[1]
            );

            if ($distance <= $radius) {
                $place['distance'] = round($distance, 1);
                $objects[]         = $place;
            }
        }

        echo json_encode($objects);
    } elseif ($_GET['action'] === 'getData') {
        $allPlaces = $dataSource->findAll(['id != 0']);

        echo json_encode($allPlaces);
    }
} else {
    readfile('views/index.html');
}

